# Docker images (not the dockerfiles)

- Docker images for Eurobin/inria.
- The dockerfiles are usually in the repo that corresponds to the project (on github)
- The images are here: https://gitlab.inria.fr/eurobin-horizon/dockers/container_registry 
